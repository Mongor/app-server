const dbSubscribe = require("../models/subscribe");
const Subscribe = dbSubscribe.subscribes;

const nodemailer = require('nodemailer');
const transporter = nodemailer.createTransport({
    service: 'gmail',
    port: 4220,
    auth :  {
        user: 'mitrobaz@gmail.com',
        // pass: ''
    }
});

exports.create = (req, res) => {

    if (!req.body.email) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    const subscribe = {
        email: req.body.email
    };

    const emailMessage = {
        from: 'mitrobaz@gmail.com',
        to: req.body.email,
        subject: 'You subscribe on NOTICEWORL',
        text: '<h1>Thx for subscribe</h1>'
    };

    Subscribe.create(subscribe)
        .then(data => {
            res.send(data);

            console.log( "email body" + emailMessage.to);

            transporter.sendMail(emailMessage, function(error, info){
                if (error) {
                  console.log(error);
                } else {
                  console.log('Email sent: ' + info.response);
                }
              });
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the Subscribe."
            });
        });

};


exports.findAll = (req, res) => {

    Subscribe.findAll({ })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving Subscribe."
            });
        });
};

exports.findOne = (req, res) => {
    const id = req.params.id;

    Subscribe.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Subscribe with id=" + id
            });
        });
};