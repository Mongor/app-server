
const dbNotice = require("../models/notice");
const dbUsers = require("../models/users");
const User = dbUsers.users;

const nodemailer = require('nodemailer');
const transporter = nodemailer.createTransport({
    service: 'gmail',
    port: 4220,
    auth :  {
        user: 'mitrobaz@gmail.com',
        // pass: ''
    }
});

exports.create = (req, res) => {

    const user = {
        email: req.body.email,
        password: req.body.password
    };

    const emailMessage = {
        from: 'mitrobaz@gmail.com',
        to: req.body.email,
        subject: 'You register on NOTICEWORL',
        text: '<h1>Thx for register</h1>'
    };

    User.create(user)
        .then(data => {
            
            res.send(data);

            transporter.sendMail(emailMessage, function(error, info){
                if (error) {
                //   console.log(error);
                } else {
                //   console.log('Email sent: ' + info.response);
                }
              });
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the User."
            });
        });

};


exports.findAll = (req, res) => {

    User.findAll({
            include: [dbNotice.notices]
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving User."
            });
        });
};


exports.findOne = (req, res) => {
    const id = req.params.id;

    User.findByPk(id, {
            include: [dbNotice.notices]
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving User with id=" + id
            });
        });
};

exports.update = (req, res) => {
    const id = req.params.id;

    User.update(req.body, {
            where: {
                id: id
            }
        })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "User was updated successfully."
                });
            } else {
                res.send({
                    message: `Cannot update User with id=${id}. Maybe User was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating User with id=" + id
            });
        });
};


exports.delete = (req, res) => {
    const id = req.params.id;

    User.destroy({
            where: {
                id: id
            }
        })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "User was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete User with id=${id}. Maybe User was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete User with id=" + id
            });
        });
};


exports.deleteAll = (req, res) => {
    User.destroy({
            where: {},
            truncate: false
        })
        .then(nums => {
            res.send({
                message: `${nums} User were deleted successfully!`
            });
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while removing all User."
            });
        });
};


exports.findAllPublished = (req, res) => {
    User.findAll({
            where: {
                published: true
            }
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving User."
            });
        });
};


exports.login = (req,res) => {
    console.log('Done');
    return res.send({
        message: 'Done'
    });

    // // // User.findOne({})
    // // .then(data => {
    //     res.send(data);
    // // })
    // // .catch(err => {
    //     res.status(500).send({
    //         message: err.message || "Some error User."
    //     // });
    // // });

}