const dbUsers = require("../models/users");
const db = require("../models/notice");
const Notice = db.notices;

exports.create = (req, res) => {
    if (!req.body.title) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    const notice = {
        title: req.body.title,
        text: req.body.text,
        phone: req.body.phone,
        published: req.body.published,
        pay_status: req.body.pay_status,
        userId: req.body.userId
    };

    Notice.create(notice)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the Notice."
            });
        });

};

exports.findAll = (req, res) => {

    Notice.findAll( {include:[dbUsers.users]} )
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving Notice."
            });
        });
};

exports.findOne = (req, res) => {
    const id = req.params.id;

    Notice.findByPk(id , {include:[dbUsers.users]})
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: "Error retrieving Notice with id=" + id
            });
        });
};

exports.update = (req, res) => {
    const id = req.params.id;

    Notice.update(req.body, {
            where: {
                id: id
            }
        })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Notice was updated successfully."
                });
            } else {
                res.send({
                    message: `Cannot update Notice with id=${id}. Maybe Notice was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Notice with id=" + id
            });
        });
};

exports.delete = (req, res) => {
    const id = req.params.id;

    Notice.destroy({
            where: {
                id: id
            }
        })
        .then(num => {
            if (num == 1) {
                res.send({
                    message: "Notice was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Notice with id=${id}. Maybe Notice was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Notice with id=" + id
            });
        });
};

exports.deleteAll = (req, res) => {
    Notice.destroy({
            where: {},
            truncate: false
        })
        .then(nums => {
            res.send({
                message: `${nums} Notice were deleted successfully!`
            });
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while removing all Notice."
            });
        });
};

exports.findAllPublished = (req, res) => {
    Notice.findAll({
            where: {
                published: true
            }
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving Notice."
            });
        });
};

exports.findAllUnpublished = (req, res) => {
    Notice.findAll({
            where: {
                published: false
            }
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving Notice."
            });
        });
};

exports.findAllPublishedPayNotice = (req, res) => {
    Notice.findAll({
            where: {
                pay_status: 1
            }
        })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving Notice."
            });
        });
};

