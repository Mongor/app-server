module.exports = (sequelize, Sequelize) => {
    const Users = sequelize.define("users", {
        id:{
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false  
        },
        name: {
            type: Sequelize.TEXT
        },
        email: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: {
                args: true,
                message: 'Email already exist'
            }
        },
        password: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        phone: {
            type: Sequelize.BIGINT
        },
        status: {
            type: Sequelize.INTEGER,
            defaultValue: 0
        }
    });

    return Users;
};