module.exports = (sequelize, Sequelize) => {
    const Notice = sequelize.define("notices", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        title: {
            type: Sequelize.STRING,
            allowNull: false
        },
        text: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        published: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        pay_status: {
            type: Sequelize.INTEGER,
            allowNull: true,
            defaultValue: 0
        }
    });

    return Notice;
};