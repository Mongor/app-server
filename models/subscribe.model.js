module.exports = (sequelize, Sequelize) => {
	const Subscribe = sequelize.define("subscribes", {
		email: {
			type: Sequelize.TEXT,
			allowNull: false,
			validate: {
				isEmail: true
			},
			unique: {
				args: true,
				message: 'Email address already in use!'
			}
		}
	});

	return Subscribe;
};