
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");


const app = express();

var corsOptions = {
	origin: "http://localhost:4200"
};

app.use(cors(corsOptions));

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({
	extended: true
}));


app.get("/", (req, res) => {
	res.json({
		message: "Server start , now go work!!!"
	});
});

const dbSubscribe = require("./models/subscribe");
dbSubscribe.sequelize.sync();
// dbSubscribe.sequelize.sync({ force: true }).then(() => {
//   console.log("Drop and re-sync dbSubscribe.");
// });
require("./routes/subscribe.routes")(app);


const dbUsers = require("./models/users");
dbUsers.sequelize.sync();
// dbUsers.sequelize.sync({ force: true }).then(() => {
//   console.log("Drop and re-sync users table.");
// });
require("./routes/user.routes")(app);

const dbNotice = require("./models/notice");
dbNotice.sequelize.sync();
// dbNotice.sequelize.sync({ force: true }).then(() => {
//   console.log("Drop and re-sync dbNotice.");
// });
require("./routes/notice.routes")(app);


const Notice = dbNotice.notices;
const Users = dbUsers.users;

Users.hasMany(Notice);
Notice.belongsTo(Users);

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
	console.log(`Server is running on port ${PORT}.`);
});