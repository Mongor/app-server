module.exports = app => {
	const notice = require("../controllers/notice.controller.js");

	var router = require("express").Router();

	router.post("/", notice.create);

	router.get("/", notice.findAll);

	router.get("/unpublished", notice.findAllUnpublished);

	router.get("/published", notice.findAllPublished);

	router.get("/payed", notice.findAllPublishedPayNotice);

	router.get("/published/:id", notice.findOne);
	
	router.put("/:id", notice.update);

	router.delete("/:id", notice.delete);

	router.delete("/", notice.deleteAll);

	app.use('/notice', router);
};