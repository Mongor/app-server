module.exports = app => {
    const subscribes = require("../controllers/subscribe.controller.js");
  
    var router = require("express").Router();
  
   
    router.post("/", subscribes.create);
  

    router.get("/", subscribes.findAll);
  
 
    router.get("/:id", subscribes.findOne);
  
    app.use('/subscribes', router);
  };